#include <ESP8266WiFi.h>
#include <PubSubClient.h>


#define WIFI_STA_NAME "BJ18-3TO-25"
#define WIFI_STA_PASS "chun111111"

#define MQTT_SERVER   "m15.cloudmqtt.com"
#define MQTT_PORT     13951
#define MQTT_USERNAME "gnsropwm"
#define MQTT_PASSWORD "9sUWUmSswUVH"
#define MQTT_NAME     "fridge"

#define LED 16


WiFiClient client;
PubSubClient mqtt(client);


void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  String topic_str = topic, payload_str = (char*)payload;
  Serial.println("[" + topic_str + "]: " + payload_str);


  if (payload_str == "fridgeon")
  {
    digitalWrite(LED, HIGH);
    mqtt.publish("/fridge", "Connect_Success");
  }
  else if (payload_str == "fridgeoff")
  {
    digitalWrite(LED, LOW);
    mqtt.publish("/fridge", "Connect_Success");
  }


}

void setup() {

  Serial.begin(115200);
  pinMode(LED, OUTPUT);


  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_STA_NAME);

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_STA_NAME, WIFI_STA_PASS);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");

  }


  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  mqtt.setServer(MQTT_SERVER, MQTT_PORT);
  mqtt.setCallback(callback);



}

void loop() {


  if (mqtt.connected() == false) {
    Serial.print("MQTT connection... ");
    if (mqtt.connect(MQTT_NAME, MQTT_USERNAME, MQTT_PASSWORD)) {
      Serial.println("connected");
      mqtt.subscribe("/fridge");
    }
    else {
      Serial.println("failed");
      delay(5000);
    }
  }
  else {
    mqtt.loop();

  }



}
