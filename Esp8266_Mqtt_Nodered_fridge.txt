[
    {
        "id": "f4e2b740.290b68",
        "type": "tab",
        "label": "Flow 1",
        "disabled": false,
        "info": ""
    },
    {
        "id": "464729ba.f3e4c8",
        "type": "http in",
        "z": "f4e2b740.290b68",
        "name": "",
        "url": "/mybot",
        "method": "get",
        "upload": false,
        "swaggerDoc": "",
        "x": 154,
        "y": 194,
        "wires": [
            [
                "b2a009cc.cdb478",
                "23e2bced.a963b4"
            ]
        ]
    },
    {
        "id": "23e2bced.a963b4",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 436,
        "y": 56,
        "wires": []
    },
    {
        "id": "e8575733.2deaa8",
        "type": "http response",
        "z": "f4e2b740.290b68",
        "name": "",
        "statusCode": "",
        "headers": {},
        "x": 590,
        "y": 194,
        "wires": []
    },
    {
        "id": "b2a009cc.cdb478",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "  var mode = '';\n  var vtoken = '';\n  var challenge = '';\n  if (msg.payload['hub.mode']) {\n      mode = msg.payload['hub.mode'];\n  }\n  if (msg.payload['hub.verify_token']) {\n      vtoken = msg.payload['hub.verify_token'];\n  }\n  if (msg.payload['hub.challenge']) {\n    challenge = msg.payload['hub.challenge'];\n  }\n  if ('subscribe' == mode &&\n    'abcde123456' == vtoken) {\n      msg.payload = challenge;\n    }\n    \nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 389,
        "y": 194,
        "wires": [
            [
                "e8575733.2deaa8"
            ]
        ]
    },
    {
        "id": "9379f03b.270c5",
        "type": "http in",
        "z": "f4e2b740.290b68",
        "name": "",
        "url": "/mybot",
        "method": "post",
        "upload": false,
        "swaggerDoc": "",
        "x": 167,
        "y": 367,
        "wires": [
            [
                "7f933129.eeb73",
                "aa1f0e87.e96cc",
                "d71b2100.3345b"
            ]
        ]
    },
    {
        "id": "aa1f0e87.e96cc",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 370,
        "y": 292,
        "wires": []
    },
    {
        "id": "7f933129.eeb73",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "if(msg.payload.entry[0].messaging[0].message.text == \"turn on fridge\")\n{\nif (msg.payload.object && 'page' == msg.payload.object) {\n    msg.processAS = 'NOTHING';\n    if (msg.payload.entry){\n        var entry = msg.payload.entry;\n\n        for (var i = 0; i < entry.length; i++) {\n            //console.log(myArray[j].x);\n            var pageID = entry[i].id;\n            var timeOfEvent = entry[i].time;\n            var messaging = entry[i].messaging\n            for (var j =0; j < messaging.length; j++) {\n                if (messaging[j].optin) {\n                    msg.processAS = 'AUTHENTICATION';     \n                } else if (messaging[j].message) {\n                  if(messaging[j].message.attachments) {\n                      if(messaging[j].message.attachments[0].type == \"location\") {\n                          msg.processAS = 'LOCATION';\n                      } else {\n                          msg.processAS = 'OTHER';\n                      }\n                  } else {\n                      msg.processAS = 'MESSAGE';\n                  }\n                } else if (messaging[j].delivery) {\n                    msg.processAS = 'DELIVERY';\n                } else if (messaging[j].postback) {\n                    msg.processAS = 'POSTBACK';\n                }\n                msg.messagingEvent = messaging[j];\n                node.send([msg,null,null]);               \n            }\n        }\n    }\n}\n}\n\nif(msg.payload.entry[0].messaging[0].message.text == \"turn off fridge\")\n{\nif (msg.payload.object && 'page' == msg.payload.object) {\n    msg.processAS = 'NOTHING';\n    if (msg.payload.entry){\n        var entry = msg.payload.entry;\n\n        for (var i = 0; i < entry.length; i++) {\n            //console.log(myArray[j].x);\n            var pageID = entry[i].id;\n            var timeOfEvent = entry[i].time;\n            var messaging = entry[i].messaging\n            for (var j =0; j < messaging.length; j++) {\n                if (messaging[j].optin) {\n                    msg.processAS = 'AUTHENTICATION';     \n                } else if (messaging[j].message) {\n                  if(messaging[j].message.attachments) {\n                      if(messaging[j].message.attachments[0].type == \"location\") {\n                          msg.processAS = 'LOCATION';\n                      } else {\n                          msg.processAS = 'OTHER';\n                      }\n                  } else {\n                      msg.processAS = 'MESSAGE';\n                  }\n                } else if (messaging[j].delivery) {\n                    msg.processAS = 'DELIVERY';\n                } else if (messaging[j].postback) {\n                    msg.processAS = 'POSTBACK';\n                }\n                msg.messagingEvent = messaging[j];\n                node.send([null,msg,null]);               \n            }\n        }\n    }\n}\n}\n\nreturn [null,null,msg];",
        "outputs": 3,
        "noerr": 0,
        "x": 373,
        "y": 378,
        "wires": [
            [
                "346b0766.74dfd8",
                "3f456211.df866e"
            ],
            [
                "c7134872.6384e8"
            ],
            [
                "e1fd00f.93cd2"
            ]
        ]
    },
    {
        "id": "346b0766.74dfd8",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "messagingEvent",
        "x": 647,
        "y": 294,
        "wires": []
    },
    {
        "id": "4cf080da.cf3af",
        "type": "facebook-messenger-writer",
        "z": "f4e2b740.290b68",
        "name": "",
        "x": 933.7500152587891,
        "y": 383.7500057220459,
        "wires": [
            [
                "d531b61f.36bb18"
            ]
        ]
    },
    {
        "id": "d531b61f.36bb18",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 1040.000015258789,
        "y": 312.777774810791,
        "wires": []
    },
    {
        "id": "e1fd00f.93cd2",
        "type": "http response",
        "z": "f4e2b740.290b68",
        "name": "",
        "statusCode": "",
        "headers": {},
        "x": 472.1944580078125,
        "y": 516.3889007568359,
        "wires": []
    },
    {
        "id": "3371bbee.8a52d4",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "var fridgeStatus = global.get(\"fridgeStatus\")\n\nif (fridgeStatus === true)\n{\nmsg.facebookevent = msg.messagingEvent;\nmsg.payload = \"Fridge is turned off!!\";\n}\nelse\n{\nmsg.facebookevent = msg.messagingEvent;\nmsg.payload = \"Connection_Error\";\n}\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 715.694408416748,
        "y": 448.47220611572266,
        "wires": [
            [
                "f058cdb0.2001c"
            ]
        ]
    },
    {
        "id": "f058cdb0.2001c",
        "type": "facebook-messenger-writer",
        "z": "f4e2b740.290b68",
        "name": "",
        "x": 924.4443511962891,
        "y": 438.472204208374,
        "wires": [
            [
                "9525a858.bedc18"
            ]
        ]
    },
    {
        "id": "9525a858.bedc18",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 1044.1666412353516,
        "y": 522.0833206176758,
        "wires": []
    },
    {
        "id": "d71b2100.3345b",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "if\n(msg.payload.entry[0].messaging[0].message.text == \"turn on fridge\")\n{msg.payload = \"fridgeon\";}\n\nelse if\n(msg.payload.entry[0].messaging[0].message.text == \"turn off fridge\")\n{msg.payload = \"fridgeoff\";}\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 403.65080070495605,
        "y": 566.825532913208,
        "wires": [
            [
                "da7ff49d.879848"
            ]
        ]
    },
    {
        "id": "da7ff49d.879848",
        "type": "mqtt out",
        "z": "f4e2b740.290b68",
        "name": "",
        "topic": "/fridge",
        "qos": "",
        "retain": "",
        "broker": "bcf530ca.2d53b",
        "x": 599.2063121795654,
        "y": 566.8254985809326,
        "wires": []
    },
    {
        "id": "eb28c3d8.ca787",
        "type": "http in",
        "z": "f4e2b740.290b68",
        "name": "",
        "url": "/HelloWorld",
        "method": "post",
        "upload": false,
        "swaggerDoc": "",
        "x": 118.33332061767578,
        "y": 726.6666355133057,
        "wires": [
            [
                "88bb9f.d2e6146",
                "8c24b70d.451a58",
                "2c29acdf.83fee4"
            ]
        ]
    },
    {
        "id": "88bb9f.d2e6146",
        "type": "switch",
        "z": "f4e2b740.290b68",
        "name": "",
        "property": "payload.request.type",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "LaunchRequest",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "IntentRequest",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "SessionEndedRequest",
                "vt": "str"
            },
            {
                "t": "else"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 4,
        "x": 311.00001525878906,
        "y": 734.4999523162842,
        "wires": [
            [],
            [
                "7adaef5.61e911"
            ],
            [],
            []
        ]
    },
    {
        "id": "8c24b70d.451a58",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 331.916690826416,
        "y": 822.0000123977661,
        "wires": []
    },
    {
        "id": "7adaef5.61e911",
        "type": "switch",
        "z": "f4e2b740.290b68",
        "name": "",
        "property": "payload.request.intent.name",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "FridgeOnIntent",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "HelpIntent",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 467.6665954589844,
        "y": 728.4999494552612,
        "wires": [
            [
                "2adfc49f.31f81c"
            ],
            []
        ]
    },
    {
        "id": "2adfc49f.31f81c",
        "type": "template",
        "z": "f4e2b740.290b68",
        "name": "",
        "field": "payload",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "Refrigerator is turned on.",
        "output": "str",
        "x": 644.8332595825195,
        "y": 723.9999446868896,
        "wires": [
            [
                "de2b594e.2e2778"
            ]
        ]
    },
    {
        "id": "de2b594e.2e2778",
        "type": "template",
        "z": "f4e2b740.290b68",
        "name": "",
        "field": "payload",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "{\n    \"version\": \"1.0\",\n    \"response\": {\n        \"outputSpeech\": {\n            \"type\": \"PlainText\",\n            \"text\": \"{{payload}}\"\n        },\n        \"shouldEndSession\":true\n    }\n}",
        "output": "str",
        "x": 832.4999542236328,
        "y": 723.4999387264252,
        "wires": [
            [
                "25317f99.aceb4"
            ]
        ]
    },
    {
        "id": "9a7dc9af.85ae78",
        "type": "http response",
        "z": "f4e2b740.290b68",
        "name": "",
        "statusCode": "",
        "headers": {},
        "x": 1178.4999656677246,
        "y": 711.9999389648438,
        "wires": []
    },
    {
        "id": "3273c8f0.516838",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload",
        "x": 1165.1666717529297,
        "y": 813.8332653045654,
        "wires": []
    },
    {
        "id": "25317f99.aceb4",
        "type": "json",
        "z": "f4e2b740.290b68",
        "name": "",
        "property": "payload",
        "action": "",
        "pretty": false,
        "x": 999.4999580383301,
        "y": 718.9999389648438,
        "wires": [
            [
                "9a7dc9af.85ae78",
                "3273c8f0.516838",
                "380990f8.fbe22"
            ]
        ]
    },
    {
        "id": "2c29acdf.83fee4",
        "type": "switch",
        "z": "f4e2b740.290b68",
        "name": "",
        "property": "payload.request.type",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "LaunchRequest",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "IntentRequest",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "SessionEndedRequest",
                "vt": "str"
            },
            {
                "t": "else"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 4,
        "x": 305.16667556762695,
        "y": 931.9999542236328,
        "wires": [
            [],
            [
                "ea671133.f96f"
            ],
            [],
            []
        ]
    },
    {
        "id": "ea671133.f96f",
        "type": "switch",
        "z": "f4e2b740.290b68",
        "name": "",
        "property": "payload.request.intent.name",
        "propertyType": "msg",
        "rules": [
            {
                "t": "eq",
                "v": "FridgeOffIntent",
                "vt": "str"
            },
            {
                "t": "eq",
                "v": "HelpIntent",
                "vt": "str"
            }
        ],
        "checkall": "true",
        "repair": false,
        "outputs": 2,
        "x": 465.58325958251953,
        "y": 929.7499523162842,
        "wires": [
            [
                "cb1a905f.49461"
            ],
            []
        ]
    },
    {
        "id": "cb1a905f.49461",
        "type": "template",
        "z": "f4e2b740.290b68",
        "name": "",
        "field": "payload",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "Refrigerator is turned off.",
        "output": "str",
        "x": 626.4999237060547,
        "y": 933.9999446868896,
        "wires": [
            [
                "3c50166c.f22f3a"
            ]
        ]
    },
    {
        "id": "3c50166c.f22f3a",
        "type": "template",
        "z": "f4e2b740.290b68",
        "name": "",
        "field": "payload",
        "fieldType": "msg",
        "format": "handlebars",
        "syntax": "mustache",
        "template": "{\n    \"version\": \"1.0\",\n    \"response\": {\n        \"outputSpeech\": {\n            \"type\": \"PlainText\",\n            \"text\": \"{{payload}}\"\n        },\n        \"shouldEndSession\":true\n    }\n}",
        "output": "str",
        "x": 814.166618347168,
        "y": 933.4999387264252,
        "wires": [
            [
                "88487101.3832d"
            ]
        ]
    },
    {
        "id": "e8691669.cfbb78",
        "type": "http response",
        "z": "f4e2b740.290b68",
        "name": "",
        "statusCode": "",
        "headers": {},
        "x": 1160.1666297912598,
        "y": 921.9999389648438,
        "wires": []
    },
    {
        "id": "f203b47a.9b9bf8",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 1146.8333358764648,
        "y": 1023.8332653045654,
        "wires": []
    },
    {
        "id": "88487101.3832d",
        "type": "json",
        "z": "f4e2b740.290b68",
        "name": "",
        "property": "payload",
        "action": "",
        "pretty": false,
        "x": 981.1666221618652,
        "y": 928.9999389648438,
        "wires": [
            [
                "e8691669.cfbb78",
                "f203b47a.9b9bf8",
                "380990f8.fbe22"
            ]
        ]
    },
    {
        "id": "380990f8.fbe22",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "if\n(msg.payload.response.outputSpeech.text == \"Refrigerator is turned on.\")\n{msg.payload = \"fridgeon\";}\n\nelse if\n(msg.payload.response.outputSpeech.text == \"Refrigerator is turned off.\")\n{msg.payload = \"fridgeoff\";}\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 404.76191329956055,
        "y": 640.7142486572266,
        "wires": [
            [
                "da7ff49d.879848"
            ]
        ]
    },
    {
        "id": "332d9b68.70d3b4",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "var fridgeStatus = global.get(\"fridgeStatus\");\n\nif (fridgeStatus === true)\n{\nmsg.facebookevent = msg.messagingEvent;\nmsg.payload = \"Fridge is turned on!!\";\n}\nelse\n{\nmsg.facebookevent = msg.messagingEvent;\nmsg.payload = \"Connection_Error\";\n}\n\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 730.000072479248,
        "y": 377.5000057220459,
        "wires": [
            [
                "4cf080da.cf3af"
            ]
        ]
    },
    {
        "id": "7d3ca705.f78398",
        "type": "mqtt in",
        "z": "f4e2b740.290b68",
        "name": "",
        "topic": "/fridge",
        "qos": "2",
        "broker": "bcf530ca.2d53b",
        "x": 882.5,
        "y": 127.5,
        "wires": [
            [
                "4a913cd2.435fe4",
                "2e46564a.cbd8ca"
            ]
        ]
    },
    {
        "id": "4a913cd2.435fe4",
        "type": "function",
        "z": "f4e2b740.290b68",
        "name": "",
        "func": "if (msg.payload == \"Connect_Success\")\n{\n    global.set(\"fridgeStatus\",true)\n}\nelse\n{\n    global.set(\"fridgeStatus\",false)\n}\n\n\nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 1056.25,
        "y": 107.5,
        "wires": [
            []
        ]
    },
    {
        "id": "c7134872.6384e8",
        "type": "delay",
        "z": "f4e2b740.290b68",
        "name": "",
        "pauseType": "delay",
        "timeout": "1",
        "timeoutUnits": "seconds",
        "rate": "1",
        "nbRateUnits": "1",
        "rateUnits": "second",
        "randomFirst": "1",
        "randomLast": "5",
        "randomUnits": "seconds",
        "drop": false,
        "x": 548.7500076293945,
        "y": 436.2500066757202,
        "wires": [
            [
                "3371bbee.8a52d4"
            ]
        ]
    },
    {
        "id": "3f456211.df866e",
        "type": "delay",
        "z": "f4e2b740.290b68",
        "name": "",
        "pauseType": "delay",
        "timeout": "1",
        "timeoutUnits": "seconds",
        "rate": "1",
        "nbRateUnits": "1",
        "rateUnits": "second",
        "randomFirst": "1",
        "randomLast": "5",
        "randomUnits": "seconds",
        "drop": false,
        "x": 567.5000076293945,
        "y": 363.7500057220459,
        "wires": [
            [
                "332d9b68.70d3b4"
            ]
        ]
    },
    {
        "id": "2e46564a.cbd8ca",
        "type": "debug",
        "z": "f4e2b740.290b68",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "false",
        "x": 1074.2857208251953,
        "y": 184.28570938110352,
        "wires": []
    },
    {
        "id": "bcf530ca.2d53b",
        "type": "mqtt-broker",
        "z": "",
        "name": "",
        "broker": "m15.cloudmqtt.com",
        "port": "13951",
        "clientid": "",
        "usetls": false,
        "compatmode": true,
        "keepalive": "60",
        "cleansession": true,
        "birthTopic": "",
        "birthQos": "0",
        "birthPayload": "",
        "closeTopic": "",
        "closeQos": "0",
        "closePayload": "",
        "willTopic": "",
        "willQos": "0",
        "willPayload": ""
    }
]